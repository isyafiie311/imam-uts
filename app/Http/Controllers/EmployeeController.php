<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use dflydev\dotaccessdata\Data;
use Illuminate\Http\Request;
use laravel\sail\console\publishcommand;


class EmployeeController extends Controller
{
    public function index(){

        $Data = Employee::all();
        return view('databuku',compact('Data'));
    }

    public function tambahdata(){
        return view('tambahdata');
    }
    
    public function insertdata( Request $request){
         //dd($request->all());
         Employee::create($request->all());
         return redirect()-route('buku');
    }

    public function tampilandata($id){

        $Data = Employee::find($id);
        //dd($Data);
        return view('tampilan', compact('Data'));
    }

    public function updatedata(Request $request, $id){
        $Data = Employee::find($id);
        $Data->updatedata($request->all());
        return redirect()-route('buku')-with('success','Data berhasil di Edit');

    }
}
