
<?php


use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EmployeeController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/buku',[EmployeeController::class,'index'])->name('buku');

Route::get('/tambahdata',[EmployeeController::class,'tambahdata'])->name('tambahdata');
Route::post('/insertdata',[EmployeeController::class,'insertdata'])->name('insertdata');

Route::get('/tampilandata/{id}',[EmployeeController::class,'tampilandata'])->name('tampilandata');
Route::post('/updatedata',[EmployeeController::class,'updatedata'])->name('updatedata');