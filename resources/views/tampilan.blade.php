<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>Bukuaja</title>
  </head>
  <body>
    <h1  class="text-center mb-4">edit data buku</h1>

        <div class="container">
            
             <div class="row justify-content-center">
                <div class="col-7">
                  <div class="card">
                    <div class="card-body">
                      <form action="/updatedata/{{ $Data-id }}" method="post" enctype="multipart/form-databuku">
                        @csrf
                        <div class="mb-3">
                          <label for="exampleInputEmail1" class="form-label">Data Perpustakaan</label>
                          <input type="text" name="dataperpustakaan" class="form-control" id="exampleInputEmail1" 
                            aria-describedby="emailHelp" volue="{{ $Data->dataperpustakaan }}">                          
                        </div>
                        <div class="mb-3">
                          <label for="exampleInputEmail1" class="form-label">Genre</label>
                          <select class="form-select" name="genre" aria-label="Default select example">
                            <option selected>{{ $Data->genre }}</option>
                            <option value="Horor">Horor</option>
                            <option value="Romansa">Romansa</option>

                          </select>                         
                        </div><div class="mb-3">
                          <label for="exampleInputEmail1" class="form-label">Kode buku</label>
                          <input type="number" name="kodebuku" class="form-control" id="exampleInputEmail1"
                             aria-describedby="emailHelp"> volue="{{ $Data->kodebuku }}">                    
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                      </form>
                    </div>
                  </div>
                </div>
                        
                           
             </div>
        </div>


  </body>
</html>