@extends('layout.master')

@section('content')
<section class="section">
  <div class="row">
  <div class="col-lg-4 col-md-4 col-sm-12">
    <div class="card card-statistic-2">
    <div class="card-stats">
      <div class="">

      </div>
    </div>
    </div>
  </div>


  <div class="container">
    <a href="/tambahdata" class="btn btn-success">Tambahkan +</a>
    <div class="row">

      <table class="table">
        <table class="table">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Dataperpustakaan</th>
                <th scope="col">Kode buku</th>
                <th scope="col">Genre</th>
                <th scope="col">Dibuat</th>
                <th scope="col">Aksi</th>
              </tr>
            </thead>
            <tbody>
            <tr>
              @foreach ($Data as $row)
              <tr>
                <th scope="row">{{ $row->id }}</th>
                <td>{{ $row->dataperpustakaan }}</td>
                <td>0{{ $row->kodebuku }}</td>
                <td>{{ $row->Genre }}</td>
                <td>{{ $row->created_at->format('D M Y') }}</td>                         
                <td>
                  <a href="/tampilkandata/{id}{{ $row->id }}" class="btn btn-info">Edit</a>
                  </td>
                </tr>
            </tr>
            @endforeach
               
          </tbody>
        </table>
    </div>
  </div>
</div>
@endsection